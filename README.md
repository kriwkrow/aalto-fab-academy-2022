# Aalto Fab Academy 2022

Landing page for the Aalto Fab Academy 2022.

# Usage

Use [Hugo](https://gohugo.io) Extended version to compile this into a website.

```bash
cd aalto-fab-academy-2022
hugo
```

You will find static HTML website compiled into `public` directory.

# ToDo

- [x] Add content
- [x] Add link to Aalto Sudios page
- [x] Adjust design for mobile (skip nav for now)
- [x] Adjust design for desktop
- [x] Navigation
- [x] Icons
- [x] Center align Pick buttons
- [x] Form functionality
- [x] Map functionality
- [x] Video
- [x] Encode videos so that sound works on mobile
- [x] Deploy and test social network integrity
- [x] Add phone number input field
- [x] Add privacy policy
