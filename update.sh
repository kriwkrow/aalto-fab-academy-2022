#!/bin/bash

# Pull and compile site using gohugo extended.
# We should have it in our project folder.

cd /var/www/aaltofab.dev.rijnieks.com/repository
/usr/bin/git pull
../hugo -b https://fablab.aalto.fi/
rm -rf ../public
mv -f public ../

cp ../videos/inspiration/* ../public/inspiration/media/
cp ../videos/background/* ../public/media/

echo "Done!"
