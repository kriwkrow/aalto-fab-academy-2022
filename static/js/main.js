var setActive = function(navLink){
  // clear 'active' class from all parent li elements
  var parent_el = navLink.parent();
  parent_el.find('a').each(function(){
    if( $(this).hasClass('active') ){
      $(this).removeClass('active');
    }
  });

  var parent_el = navLink.parent();
  navLink.addClass('active');
};

var navInit = function(){

  // Change clicked nav element to active
  $('nav a').click(function(event){
    setActive($(this));
  });

  // Handle links on page to set nav buttons to active
  $('a.nav-set').click(function(event){
    var href = $(this).attr('href');
    var navLink = $('nav a[href="' + href + '"]');
    setActive(navLink);
  });
};

var pricingInit = function(){
  $('table tfoot a').click(function(){
    var selectOption = $(this).data('select');
    var radioToSelect = $('#' + selectOption);
    radioToSelect.prop("checked", true);
  });
};

$(document).ready(function(){
  navInit();
  pricingInit();
});
