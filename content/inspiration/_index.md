+++

title = "Get inspired by this year's students"

[[project]]
name = "Axol Bot"
author = "Cuautli Garcia"
description = "Modular robot from 3D printable parts"
video = "cuautli-garcia.mp4"
poster = "axol-bot.jpg"

[[project]]
name = "Indispensable"
author = "Griffin Vanhorne"
description = "Smart pill case"
video = "griffin-vanhorne.mp4"
poster = "indispensable.jpg"

[[project]]
name = "Maker Bar"
author = "Brandon Witter"
description = "At home coktail mixer"
video = "brandon-witter.mp4"
poster = "maker-bar.jpg"

[[project]]
name = "Nova"
author = "Nadieh Bremer"
description = "Puzzle with light"
video = "nadieh-bremer.mp4"
poster = "nova.jpg"

+++

Fab Academy offers a distributed rather than distance educational model, students learn in local workgroups, with peers, mentors, and machines, which are then connected globally by content sharing and video for interactive classes.

Take a look at what our studeents have created in 2021.
