+++

title = "Learn how to make (almost) anything"
image = "wires.jpg"

[cta]
  text = "Register now"

+++

Fab Academy is a hands-on rapid prototyping course. Participants learn a true full stack of skills to be able to create high quality proof-of-concept prototypes.

At the core of the Fab Academy is the idea that art, design, and engineering should not be separate fields. Instead, students are encouraged to learn a broad range of skills to accomplish complex goals on their own.

It is a unique learning experience with live online lectures and connects hundreds of students all around the world from Boston, in the USA, to Kamakura in Japan.
