+++

title = "Home"
tags = ["CNC Machining", "CAD", "Engineering", "Electronics Design", "Web Development", "Git", "And more..."]

[cta]
  lead = "Registration for Fab Academy 2025 is open!"
  text = "Register here"
  href = "https://fabacademy.org/apply/registration.html"

[continue]
  text = "Continue to Aalto Fablab"
  href = "https://studios.aalto.fi/fablab/"

+++

You will disover a world of rapid prototyping in just 6 months. Every year from January to June.
