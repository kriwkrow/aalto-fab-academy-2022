+++

title = "Privacy Policy"

+++

Aalto Fab Academy is a project by [Aalto Studios](https://studios.aalto.fi). This privacy policy will explain how our organization uses the personal data we collect from you when you use this website.

### What data do we collect?

Personal identification information (name and email address) and data related to your choice (pricing option, comments).

### How do we collect your data?

We collect data and process data when you fill the form labeled Join Now.

### How will we use your data?

We collect your data to be able to reach out to you with more information about Aalto Fab Academy.

### How do we store your data?

The data you provide is transferred to us via email, using a forwarding service from <a href="https://formspree.io" target="_blank">Formspree</a>. This means the data is stored on Aalto University email servers and Aalto Fab Academy Formspree account. ProtonMail and Formspree uses sufficient encryption and security measures to make sure that the data is available only to us.

We will keep your submission data for one year. Once the time period has expired, we will delete your data by clearing it from Aalto University email servers, Formspree account and local storage.

### Marketing

We will inform you about any further activities regarding the Aalto Fab Academy project.

You have the right at any time to stop Our Company from contacting you for marketing purposes. If you no longer wish to be contacted for marketing purposes, please contact us via email.

### What are your data protection rights?

Our Company would like to make sure you are fully aware of all of your data protection rights. Every user is entitled to the following:

**The right to access**  
You have the right to request Our Company for copies of your personal data.

**The right to rectification**  
You have the right to request that Our Company correct any information you believe is inaccurate. You also have the right to request Our Company to complete the information you believe is incomplete.

**The right to erasure**  
You have the right to request that Our Company erase your personal data, under certain conditions.

**The right to restrict processing**  
You have the right to request that Our Company restrict the processing of your personal data, under certain conditions.

**The right to object to processing**  
You have the right to object to Our Company’s processing of your personal data, under certain conditions.

**The right to data portability**  
You have the right to request that Our Company transfer the data that we have collected to another organization, or directly to you, under certain conditions.

If you make a request, we have one month to respond to you. If you would like to exercise any of these rights, please contact us at our email: fabacademy at aalto.fi

### Cookies

Cookies are text files placed on your computer to collect standard Internet log information and visitor behavior information. We do not use cookies on our website.

For further information, visit <a href="https://allaboutcookies.org" target="_blank">allaboutcookies.org</a>.

### Privacy policies of other websites

The Our Company website contains links to other websites. Our privacy policy applies only to our website, so if you click on a link to another website, you should read their privacy policy.

### Changes to our privacy policy

Our Company keeps its privacy policy under regular review and places any updates on this web page. This privacy policy was last updated on 11 December 2021.

### How to contact us

If you have any questions about Our Company’s privacy policy, the data we hold on you, or you would like to exercise one of your data protection rights, please do not hesitate to contact us.

Email us at: fabacademy @ aalto.fi
