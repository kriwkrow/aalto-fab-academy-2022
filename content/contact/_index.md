+++

title = "Contact us"
image = "waterjet.jpg"

[form]
action = "https://formspree.io/f/xnqwqjrl"
first_name = "First name"
last_name = "Last name"
email = "Email"
phone = "Phone"
comments = "Do you have any questions?"
submit = "Send message"

[privacy]
text = "I have read and agree to the data privacy policy"
link_href = "#privacy"
link_text = "privacy policy"

+++

Please get in touch with us if you have any questions or suggestions. Let us know if you are considering to join the Fab Academy next year. The earlier you consider, the more time we have to make your experience as smooth as possible.
