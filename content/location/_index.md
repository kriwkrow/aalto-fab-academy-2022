+++

title = "Location"

[map]
src = "map.jpg"
alt = "Aalto Fablab location"

[visit]
title = "Visit us!"
description = "You want to learn more about how Fab Academy works and want to meet us? Make sure that we are available."
link_text = "Schedule a tour!"
link_href = "https://outlook.office365.com/owa/calendar/AaltoFablabOrientation@aaltofi.onmicrosoft.com/bookings/s/K3MrcZyDrkq_6YPX7JPKSA2"

+++

Fab Academy runs in more than 70 Fab Labs, for more than 250 students per year in the largest campus of the world.

Aalto FabLab is located in Otakaari 7 at the heart of Aalto University. There are plenty of bus public transportation options around.
