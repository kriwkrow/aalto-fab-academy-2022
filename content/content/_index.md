+++

title = "Content of the Academy"

[cta]
  text = "Join now!"
  href = "#join-now"

[[topic]]
name = "Principles and Practices"
icon = "icon-principles.svg"

[[topic]]
name = "Project Management"
icon = "icon-management.svg"

[[topic]]
name = "Computer-Aided Design"
icon = "icon-cad.svg"

[[topic]]
name = "Computer-Controlled Cutting"
icon = "icon-cutting.svg"

[[topic]]
name = "Electronics Production"
icon = "icon-electronics-production.svg"

[[topic]]
name = "Computer-Controlled Machining"
icon = "icon-machining.svg"

[[topic]]
name = "Electronics Design"
icon = "icon-electronics-design.svg"

[[topic]]
name = "Molding and Casting"
icon = "icon-molding.svg"

[[topic]]
name = "Composites"
icon = "icon-composites.svg"

[[topic]]
name = "Embedded Programming"
icon = "icon-embedded.svg"

[[topic]]
name = "3D Scanning and Printing"
icon = "icon-3d.svg"

[[topic]]
name = "Input Devices"
icon = "icon-input.svg"

[[topic]]
name = "Interface and Application Programming"
icon = "icon-interface.svg"

[[topic]]
name = "Mechanical Design"
icon = "icon-mechanical.svg"

[[topic]]
name = "Output Devices"
icon = "icon-output.svg"

[[topic]]
name = "Networking and Communications"
icon = "icon-networking.svg"

[[topic]]
name = "Machine Design"
icon = "icon-machine.svg"

[[topic]]
name = "Applications and Implications"
icon = "icon-applications.svg"

[[topic]]
name = "Project Development"
icon = "icon-development.svg"

[[topic]]
name = "Invention, Intellectual Property, and Income"
icon = "icon-intellectual.svg"

+++

Fab Acadeby goes on from mid-January to end-June every year. These are 18 weeks of individual topics ranging from web development and version control to 3D modelling, 3D printing, electronics and embedded programming.

Students have to build their websites and document their progress using Git and GitLab. Make new friends during the course and meet them live at the annual Fab Conference!
