+++

title = "Pricing"

[[option]]
name = "Onsite"
price = "6000€"
link_text = "Pick"
link_href = "#join-now"

[[option]]
name = "Remote"
price = "4500€"
link_text = "Pick"
link_href = "#join-now"

[[feature]]
name = "Live video lectures"
onsite = true
remote = true

[[feature]]
name = "Recorded video lectures"
onsite = true
remote = true

[[feature]]
name = "Local evaluation"
onsite = true
remote = true

[[feature]]
name = "Fab Academy diploma"
onsite = true
remote = true

[[feature]]
name = "Lab access"
onsite = true
remote = false

[[feature]]
name = "Materials"
onsite = true
remote = false

+++

There are two pricing options available: onsite and remote. Use the table below to compare them and choose the one that works for you best.

Fab Academy is open to potential students both coming from technical and non-technical backgrounds.
If you have a Low or Medium proficiency in 2D and 3D modeling, Digital fabrication, Electronics programming and Web design and development, the course should be considered a full-time dedication program.
